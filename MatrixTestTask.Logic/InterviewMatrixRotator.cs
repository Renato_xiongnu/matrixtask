﻿using System;
using System.Collections.Generic;
using System.Linq;
using MatrixTestTask.Logic.Helper;

namespace MatrixTestTask.Logic
{
    public class InterviewMatrixRotator : IMatrixRotator
    {

        /// <summary>
        /// Limited implementation of RotateMatrix. For more effective solution, use method: RotateMatrix
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="r"></param>
        /// <returns></returns>
        public int[][] Rotate(int[][] matrix, int r)
        {
            var arrCloned = GenericCopier<int[][]>.DeepCopy(matrix);

            for (int i = 0; i < r; i++)
            {
                RotateMatrixWithOneStep(ref arrCloned);
            }

            return arrCloned;
        }



        /// <summary>
        /// This methods rotates matrix faster, however it does not rotate it step by step
        /// </summary>
        /// <param name="arr"></param>
        /// <param name="rotator"></param>
        /// <returns></returns>
        public int[][] RotateMatrix(int[][] arr, int rotator)
        {
            var arrCloned = GenericCopier<int[][]>.DeepCopy(arr);

            int start_i = 0;
            int start_j = 0;
            int end_i = arrCloned.GetLength(0);
            int end_j = arrCloned[0].GetLength(0);

            int halfN = end_i / 2;
            int halfM = end_j / 2;

            for (int i = 1; i <= Math.Min(halfN, halfM); i++, start_i++, start_j++, end_i--, end_j--)
            {
                var queue = GetContour(arrCloned, start_i, start_j, end_i, end_j);
                RotateQueue(ref queue, rotator);
                ConvertQueueToMatrix(ref arrCloned, queue, start_i, start_j, end_i, end_j);
            }//for

            return arrCloned;
        }




        #region "Private Methods"

        /// <summary>
        /// Get countour of the matrix
        /// </summary>
        /// <param name="arr"></param>
        /// <param name="start_i"></param>
        /// <param name="start_j"></param>
        /// <param name="max_n"></param>
        /// <param name="max_m"></param>
        /// <returns></returns>
        private LinkedList<int> GetContour(int[][] arr, int start_i, int start_j, int max_n, int max_m)
        {
            var queue = new LinkedList<int>();

            int i = start_i;
            int j = start_j;
            bool isLooping = true;
            bool isHorizontal = false;

            while (isLooping)
            {
                queue.AddLast(arr[i][j]);

                if (!isHorizontal && i == start_i)
                {
                    j++;
                    if (j == max_m)
                    {
                        isHorizontal = true;
                        j = max_m - 1;
                    }
                }

                if (isHorizontal && j == max_m - 1)
                {
                    i++;
                    if (i == max_n)
                    {
                        isHorizontal = false;
                        i = max_n - 1;
                    }
                }

                if (!isHorizontal && i == max_n - 1)
                {
                    j--;
                    if (j == start_j - 1)
                    {
                        isHorizontal = true;
                        j = start_j;
                    }
                }

                if (isHorizontal && j == start_j)
                {
                    i--;
                    if (i == start_i)
                    {
                        isLooping = false;
                    }
                }
            }//while

            return queue;
        }

        /// <summary>
        /// Rotate queue/list
        /// </summary>
        /// <param name="queue"></param>
        /// <param name="rotator"></param>
        private void RotateQueue(ref LinkedList<int> queue, int rotator)
        {
            for (int k = 0; k < rotator; k++)
            {
                LinkedListNode<int> el = queue.Last;
                queue.RemoveLast();
                queue.AddFirst(el);
            }
        }

        /// <summary>
        /// Applies converted queue to the matrix
        /// </summary>
        /// <param name="arr"></param>
        /// <param name="queue"></param>
        /// <param name="start_i"></param>
        /// <param name="start_j"></param>
        /// <param name="max_n"></param>
        /// <param name="max_m"></param>
        private void ConvertQueueToMatrix(ref int[][] arr, LinkedList<int> queue, int start_i, int start_j, int max_n, int max_m)
        {
            int i = start_i;
            int j = start_j;
            bool isHorizontal = false;

            while (queue.Count > 0)
            {
                int el = queue.First();
                queue.RemoveFirst();
                arr[i][j] = el;

                if (!isHorizontal && i == start_i)
                {
                    j++;
                    if (j == max_m)
                    {
                        isHorizontal = true;
                        j = max_m - 1;
                    }
                }

                if (isHorizontal && j == max_m - 1)
                {
                    i++;
                    if (i == max_n)
                    {
                        isHorizontal = false;
                        i = max_n - 1;
                    }
                }

                if (!isHorizontal && i == max_n - 1)
                {
                    j--;
                    if (j == start_j - 1)
                    {
                        isHorizontal = true;
                        j = start_j;
                    }
                }

                if (isHorizontal && j == start_j)
                {
                    i--;
                }
            }//while
        }


        /// <summary>
        /// Limited implementation of RotateMatrix
        /// </summary>
        /// <param name="arr"></param>
        private void RotateMatrixWithOneStep(ref int[][] arr)
        {
            int start_i = 0;
            int start_j = 0;
            int end_i = arr.GetLength(0);
            int end_j = arr[0].GetLength(0);

            int halfN = end_i / 2;
            int halfM = end_j / 2;

            int counter = 0;
            int rotator = 1;//rotation is limitted to conform the requirements

            for (int i = 1; i <= Math.Min(halfN, halfM); i++, start_i++, start_j++, end_i--, end_j--, counter++)
            {
                var queue = GetContour(arr, start_i, start_j, end_i, end_j);
                RotateQueue(ref queue, rotator);
                ConvertQueueToMatrix(ref arr, queue, start_i, start_j, end_i, end_j);
            }//for
        }

        #endregion

    }//class
}
