﻿namespace MatrixTestTask.Logic
{
    public interface IMatrixRotator
    {
        int[][] Rotate(int[][] matrix, int r);
    }
}
