﻿using MatrixTestTask.Logic;
using System;


namespace MatrixTestTask
{
    class Program
    {
        private static int n = 6;
        private static int m = 4;

        private static int[][] arr = new int[n][];


        private static void FillMatrix(int[][] arr)
        {
            int counter = 0;

            for (int k = 0; k < n; k++)
            {
                arr[k] = new int[m];
            }

            for (int j = 0; j < m; j++)
            {
                for (int i = 0; i < n; i++)
                {
                    arr[i][j] = counter;
                    counter++;
                }
            }
        }


        private static void PrintMatrix(int[][] arr)
        {
            for (int j = 0; j < m; j++)
            {
                for (int i = 0; i < n; i++)
                {
                    if (arr[i][j] < 10)
                    {
                        Console.Write(" {0} ", arr[i][j]);
                    }
                    else
                    {
                        Console.Write("{0} ", arr[i][j]);
                    }
                }
                Console.Write(Environment.NewLine);
            }
        }


        static void Main(string[] args)
        {
            //Fill Matrix with initial values and display:
            FillMatrix(arr);
            PrintMatrix(arr);
    

            var logic = new InterviewMatrixRotator();

            Console.WriteLine("==== Result ========");
            var result = logic.Rotate(arr, 2);
            PrintMatrix(result);

            Console.WriteLine("==== Result2 ========");
            var result2 = logic.RotateMatrix(arr, 2);
            PrintMatrix(result2);


            Console.ReadKey();

        }//main

    }//class
}
